@echo off

set MY_PATH=%~dp0
:: Strip off final backslash
set MY_PATH=%MY_PATH:~0,-1%

:: Set Defaults
set VERSION=6.1.1
set ENVIRONMENT=pro
set ARCH=x64Win64VS2017

:: Parse Arguments
:PARSE_ARGS
if not "%1"=="" (
	if "%1" == "-v" (
		set VERSION=%2
		shift
	)
	
	if "%1" == "-e" (
		set ENVIRONMENT=%2
		shift
	)
	
	if "%1" == "-a" (
		set ARCH=%2
		shift
	)
	
	if "%1" == "-s" (
		set SECURITY=1
	)
	
	shift
	goto :PARSE_ARGS
)

:: In event the primary version of software (e.g. micro) differs from the pro
:: version of software, create a separate variable to store the pro version
set RTI_PRO=%VERSION%

goto :%ENVIRONMENT%

	:pro
		call %MY_PATH%\batch_subs\sub_pro.bat
		goto :END_ENVIRONMENT
		
	:micro
		call %MY_PATH%\batch_subs\sub_micro.bat		
		goto :pro
		
	:drive
		call %MY_PATH%\batch_subs\sub_drive.bat
		goto :pro
	
:END_ENVIRONMENT

::set VERSION=%VERSION:~0,1%.%VERSION:~1,1%.%VERSION:~2,1%

:: Cleanup temporary variables
set "VERSION="
set "RTI_PRO="
set "MY_PATH="
set "SECURITY="
set "ENVIRONMENT="