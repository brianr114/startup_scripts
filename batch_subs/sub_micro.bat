@echo off

set RTIMEARCH=%ARCH%

:: Set Core Paths
set RTIMEHOME=C:\Users\breiche\Documents\rti_workspace\micro\rti_connext_dds_micro-%VERSION%
set PATH=%RTIMEHOME%\rtiddsgen\scripts;%RTIMEHOME%\lib\%RTIMEARCH%;%PATH%

goto :%VERSION%
	:3.0.1
		set RTI_PRO=6.0.0
		goto :CONNEXT_PRO_END
	:3.0.2
	:3.0.3
		set RTI_PRO=6.0.1
		goto :CONNEXT_PRO_END
	:2.4.14
		set "RTI_PRO="
:CONNEXT_PRO_END