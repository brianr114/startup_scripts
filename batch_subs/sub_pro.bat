@echo off

if not [%RTI_PRO%]==[] (
	:: Set Core Paths
	call "C:\Program Files\rti_connext_dds-%RTI_PRO%\resource\scripts\rtisetenv_%ARCH%.bat"
)

if "%SECURITY%" == "1" (
	call %MY_PATH%\batch_subs\sub_security.bat
)