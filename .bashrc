# colors!
red="\[\033[38;5;203m\]"
light_blue="\[\033[38;05;38m\]"
white="\[\033[1;37m\]"
reset="\[\033[0m\]"

parse_git_branch() {
     git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'
}

export GIT_PS1_SHOWDIRTYSTATE=1

# '\u' adds the name of the current user to the prompt
# '\$(__git_ps1)' adds git-related stuff
# '\W' adds the name of the current directory
export PS1="$red\u $white\w$light_blue\$(parse_git_branch)$white
$ "

copy_type(){
     cp "$HOME/Documents/type.idl" .
}

vsc(){
     code *.code-workspace
}

set_env(){
    	#source "$HOME/rti_connext_dds-${1:0:1}.${1:1:1}.${1:2:1}/resource/scripts/rtisetenv_${2}.bash"	 
	 
	# Set Defaults
	VERSION=6.1.1
	ENVIRONMENT=pro
	ARCH=x64Linux4gcc7.3.0
	
	while [ "$#" -gt 0 ]; do
		case $1 in 
			-v)
				VERSION=$2;
				shift;
			;;
			-e)
				ENVIRONMENT=$2;
				shift;
			;;
			-a)
				ARCH=$2;
				shift;
			;;
			*)
				# Do something for default case
			;;
		esac
		shift;
	done
	
	RTI_PRO=$VERSION
	
	case $ENVIRONMENT in
		micro)
			set_micro_env
		;;
		drive)
			case $VERSION in
				2.0.0)
				RTI_PRO=6.1.1
				;;
			esac
		;;	
	esac
	
	set_pro
}

set_pro(){
	# Set Connext Pro configuration
	source "$HOME/rti_connext_dds-${RTI_PRO}/resource/scripts/rtisetenv_${ARCH}.bash"
}

set_micro_env(){
     # Get version of RTI Pro
     case ${VERSION} in
     
       3.0.1)
         RTI_PRO=6.0.0
         ;;
         
       3.0.2 | 3.0.3)
         RTI_PRO=6.0.1
         ;;
         
     esac
        
     # Set environment variables for RTI Micro
     export RTIMEARCH=${ARCH}
     export RTIMEHOME="$HOME/micro/rti_connext_dds_micro-${VERSION}"
     export PATH=$RTIMEHOME/rtiddsgen/scripts:$RTIMEHOME/lib/$RTIMEARCH:$PATH
}